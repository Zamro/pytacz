#include "PytaczWindow.hpp"
#include "ui_PytaczWindow.h"
#include "mainwindow.hpp"

#include <random>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QMessageBox>

enum Commands{
    Languages,
    SpecialCharacters
};

const std::map<QString, Commands> commandsMap = {
    {"!lng", Commands::Languages},
    {"!spe", Commands::SpecialCharacters}
};

PytaczWindow::PytaczWindow(QWidget *parent) :
    QWidget(parent),
    generator(std::chrono::system_clock::now().time_since_epoch().count()),
    ui(new Ui::PytaczWindow)
{
    ui->setupUi(this);
}

void PytaczWindow::setData(QString filename, int repeats, bool engPol, bool polEng, int entryLearning)
{
    this->entryLearning = entryLearning;
    clear();
    readData("data/" + filename);
    auto initialData = std::move(data.list);
    data.list = {};
    for(auto& record : initialData )
    {
        if(polEng)
            data.list.push_back({record.second,record.first});
        if(engPol)
            data.list.push_back(std::move(record));
    }
    counters = std::vector<unsigned>(data.list.size(), (unsigned)repeats);
    setSpecialCharacterButtons();
    ask();
}

PytaczWindow::~PytaczWindow()
{
    delete ui;
}

void PytaczWindow::on_exit_clicked()
{
    emit stopped();
}

void PytaczWindow::parseCommand(const QString& line, QuestionsData& data)
{
    auto words = line.split(";");
    if(words.size() >= 3 && commandsMap.count(words[0]) > 0)
        switch(commandsMap.at(words[0]))
        {
        case Commands::Languages:
            break;
        case Commands::SpecialCharacters:
            data.specialCharacters = {std::move(words[1]), std::move(words[2])};
            break;
        }
}

void PytaczWindow::addLineToDataIfValid(const QString& line, QuestionsData& data)
{
    auto words = line.split(";");
    if(words.size() >= 2)
    {
        data.list.push_back({words[0].trimmed(), words[1].trimmed()});
    }
}

void PytaczWindow::readData(QString filename)
{
    QFile file(filename);
    file.open(QIODevice::ReadOnly);
    QTextStream in(&file);
    while(!in.atEnd())
    {
        QString line = in.readLine().trimmed();
        if(line != "" && line[0] != '#')
        {
            if(line[0] == '!')
                parseCommand(line, data);
            else{
                addLineToDataIfValid(line, data);
//                qDebug()<<"Added: "<<data.list.back().first<<" "<<data.list.back().second;
            }
        }
    }
}

void PytaczWindow::clear()
{
    ui->questionAnswerField->clear();
    ui->properAnswerField->clear();
    ui->yourAnswerField->clear();
    ui->answersLeft->clear();
    ui->answerWidget->setAutoFillBackground(false);
    goodAnswers = 0;
    badAnswers = 0;
    data = QuestionsData();
    index = -1;
}

void PytaczWindow::ask(){
    if(data.list.size())
    {
        int size = entryLearning ? std::min(entryLearning, int(data.list.size())) : data.list.size();
        std::uniform_int_distribution<int> distribution(0, size - 1);
        auto old_index = index;
        do{
            index = distribution(generator);
        }while(size > 1 && old_index == index);
        ui->lineEdit->setText(data.list[index].first);
        ui->lineEdit->update();
        ui->answerField->clear();
        ui->answerField->setFocus();
    }
    else
    {
        QMessageBox::information(this,"Gratulacje",
                                 "Gratulacje, przypytywanie zakończone.\nOdpowiedziałeś poprawnie " +
                                 QString::number(goodAnswers) + " razy oraz niepoprawnie " +
                                 QString::number(badAnswers) + " razy","ok");
        on_exit_clicked();
    }
}

void PytaczWindow::check(){
    ui->questionAnswerField->setText(data.list[index].first);
    ui->properAnswerField->setText(data.list[index].second);
    ui->yourAnswerField->setText(ui->answerField->text());

    QPalette pal(palette());
    QStringList properAnswers = ui->properAnswerField->text().split("/");

    if(properAnswers.contains(ui->yourAnswerField->text().trimmed(),Qt::CaseInsensitive)){
        counters[index]--;
        goodAnswers++;
        pal.setColor(QPalette::Background, Qt::green);
    }
    else{
        counters[index]++;
        badAnswers++;
        pal.setColor(QPalette::Background, Qt::red);
    }
    ui->answerWidget->setAutoFillBackground(true);
    ui->answerWidget->setPalette(pal);
    ui->answersLeft->setValue(counters[index]);

    if(counters[index] == 0)
    {
        counters.erase(counters.begin() + index);
        data.list.erase(data.list.begin() + index);
        index = -1;
    }

    ask();
}

void PytaczWindow::setSpecialCharacterButtons()
{
//    qDebug()<<"lol2: "<<data.specialCharacters.first<<" "<<data.specialCharacters.second;
    unsigned columns = 4;
    unsigned i=0;
    for(auto character : data.specialCharacters.first)
    {
        auto* parent = (QGridLayout*)ui->specialCharacterButtonsPanelOne->layout();
        QPushButton* characterButton = new QPushButton(character);
        connect(characterButton, SIGNAL(clicked()), this, SLOT(specialCharacterButtonPressed()));
        parent->addWidget(characterButton, i/columns, i%columns);
        i++;
    }
    i = 0;
    for(auto character : data.specialCharacters.second)
    {
        auto* parent = (QGridLayout*)ui->specialCharacterButtonsPanelTwo->layout();
        QPushButton* characterButton = new QPushButton(character);
        connect(characterButton, SIGNAL(clicked()), this, SLOT(specialCharacterButtonPressed()));
        parent->addWidget(characterButton, i/columns, i%columns);
        i++;
    }
}

void PytaczWindow::on_pushButton_clicked()
{
    check();
}

void PytaczWindow::on_answerField_returnPressed()
{
    check();
}

void PytaczWindow::specialCharacterButtonPressed()
{
    QPushButton* button = qobject_cast<QPushButton*>( sender() );

    if ( button )
    {
        ui->answerField->insert( button->text() );
        ui->answerField->setFocus();
    }
}
