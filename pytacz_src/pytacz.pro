#-------------------------------------------------
#
# Project created by QtCreator 2016-08-02T20:42:50
#
#-------------------------------------------------

QT       += core gui
CONFIG   += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pytacz
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    PytaczWindow.cpp

HEADERS  += mainwindow.hpp \
    PytaczWindow.hpp

FORMS    += mainwindow.ui \
    PytaczWindow.ui
