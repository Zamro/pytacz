#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_checkBox_clicked(bool checked);

    void on_checkBox_2_clicked(bool checked);

    void on_isEntryLearning_clicked(bool checked);

    void on_startButton_clicked();

    void on_page_2_stopped();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_HPP
