#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include <QDir>
#include <QtDebug>
#include <QCollator>
#include "PytaczWindow.hpp"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QDir dir("data");
    auto files = dir.entryList(QStringList("*.txt"));
    QCollator collator;
    collator.setNumericMode(true);

    std::sort(
        files.begin(),
        files.end(),
        [&collator](const QString &file1, const QString &file2)
        {
            return collator.compare(file1, file2) < 0;
        });
    ui->comboBox->addItems(files);
    ui->comboBox->update();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_checkBox_clicked(bool checked)
{
    ui->checkBox_2->setEnabled(checked);
}

void MainWindow::on_checkBox_2_clicked(bool checked)
{
    ui->checkBox->setEnabled(checked);
}

void MainWindow::on_isEntryLearning_clicked(bool checked)
{
    ui->entyLearningValue->setEnabled(checked);
}

void MainWindow::on_startButton_clicked()
{
    ui->page_2->setData(ui->comboBox->currentText(),
                        ui->spinBox->value(),
                        ui->checkBox->isChecked(),
                        ui->checkBox_2->isChecked(),
                        ui->isEntryLearning->isChecked() * ui->entyLearningValue->value());
    ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::on_page_2_stopped()
{
    ui->stackedWidget->setCurrentIndex(0);
}
