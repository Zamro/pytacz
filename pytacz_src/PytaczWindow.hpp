#ifndef PYTACZWINDOW_HPP
#define PYTACZWINDOW_HPP

#include <QWidget>
#include <vector>
#include <chrono>

namespace Ui {
class PytaczWindow;
}

struct QuestionsData{
    std::vector<std::pair<QString, QString>> list;
    std::pair<QString, QString> specialCharacters;
};

class PytaczWindow : public QWidget
{
    Q_OBJECT
    QuestionsData data;
    void readData(QString filename);
    std::vector<unsigned> counters;
    void clear();
    void ask();
    void check();
    int index = -1;
    int badAnswers;
    int goodAnswers;
    int entryLearning = 0;

    std::default_random_engine generator;

public:
    explicit PytaczWindow(QWidget *parent = 0);
    void setData(QString filename, int repeats, bool engPol, bool polEng, int entryLearning);
    ~PytaczWindow();

private slots:
    void on_exit_clicked();

    void on_pushButton_clicked();

    void on_answerField_returnPressed();
    void specialCharacterButtonPressed();

private:
    Ui::PytaczWindow *ui;
    void parseCommand(const QString&, QuestionsData&);
    void addLineToDataIfValid(const QString&, QuestionsData&);
    void setSpecialCharacterButtons();

signals:
    void stopped();
};

#endif // PYTACZWINDOW_HPP
