﻿!lng;español/hiszpański;polaco/polski
!spe;¿¡áÁéÉíÍñÑóÓúÚüÜ;ąĄćĆęĘłŁóÓśŚżŻźŹ
decir;powiedzieć
abrir;otwierać
la pagina;strona
poder;móc
puedo;mogę
salir;wychodzić
antes;przed
despúes;po
el queso;ser
el hambre;głód
horroroso;straszny
la extranjera;cudzoziemka
la ducha;prysznic
el apellido;nazwisko
conocer;znać
conozco;znam
el cuento;bajka
comer;jeść
el otoño;jesień
en;w
de;z(skąd?)
con;z(z kim/czym?)
o;albo/lub
sobre;o
ceñar;jeść na kolację
escuchar;słuchać
correr;biec
rápido;szybko
para;aby
robar;kraść
el aceite;oliwa
la aceituna;oliwka
el tomate;pomidor
el vino;wino
el resultado;wynik
la historia;historia
la influencia;wpływ
la palabra;słowo
venir;przybywać
por ejemplo;na przykład
el resto;reszta
las otras lenguas;inne języki
el arroz;ryż
la taza;filiżanka
indígeno;tubylczy
también;również
hoy;dzisiaj
el origen;źródło
el champú;szampon
el champán;szampan
el perro;pies
la casa;dom
la flor;kwiat
la mesa;stół
el domingo;niedziela
el estudiante;student
la estudiante;studentka
el sol;słońce
el map;mapa
el chico;chłopak
la chica;dziewczyna
la universidad;uniwersytet
preparar;przygotowywać
esparar;czekać
la playa;plaża
el concierto;koncert
la ciudad;miasto
díga;proszę mówić
dígame;proszę słuchać
socorro;pomocy
pareja;para
saber;umieć
#języki 21.10
la idioma;język
el español;hiszpański/hiszpan
el inglés;angielski
el polaco;polski
el griego;grecki
el alemán;niemiecki
el francés;francuski
el chino;chiński
el ruso;rosyjski
el japonés;japoński
#kierunki studiów 21.10
mecánica y construcción de maquinas;mechanika i budowa maszyn
construcción;budownictwo
electrotécnica;elektrotechnika
gestion e ingenieria de producción;zarządzanie i inżynieria produkcji
ingenieria medioambiental;inżynieria środowiska
informatica;informatyka
ciencias de computación;informatyka(computer science)
automática y robótica;automatyka i robotyka
energética;energetyka
gestión de empresas;zarządzanie przedsiębiorstwem
pasar;spędzać/przechodzić
las vacaciones;wakacje
la ropa;ciuch
hacer;robić
el soldado;żołnierz
el abogado;adwokat
el representante comercial;reprezentant handlowy
el recaudador;komornik
el politico;polityk
el supermercado;supermarket
el operario;pracownik fizyczny
la oficina;biuro
el secretario;sekretarz
el arquitecto;architekt
la empresa;firma
el banco;bank
el cajero;kasjer
jubilado;emerytowany
en paro;na bezrobociu
buscar;poszukiwać
anprender;nauczyć się
bibir;pić
comer;jeść
autoritario;autorytatywny
aqui;tutaj
donde;gdzie
es que;chodzi o to, że
oír;słyszeć
oye;słuchasz/słuchaj
el reloy;zegarek
surfir;cierpieć
el miembro de familia;członek rodziny
el dinero;pieniądze
la cerveza;piwo
el año;rok
buscar;szukać
barato;tani
el amigo;przyjaciel
la cámara;kamera
pero;ale
ahora;teraz
la obra;akcja/dzieło/budowa
la carta;list
además;ponadto
porque;ponieważ
siempre;zawsze
la cosa;rzecz
poco;trochę
el mañana;poranek
la mañana;jutro
intercambiar;wymieniać
el tema;temat
la idea;pomysł
el lugar;miejsce
la ama de casa;gospodyni domowa
preguntar;pytać
la gente;ludzie
el pueblo;społeczność
todo;cały
ya;już
país;kraj