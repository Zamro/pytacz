﻿!lng;english/angielski;polish/polski
!spe;;ąĄćĆęĘłŁóÓśŚżŻźŹ
#This are the words to the text found at:
#https://www.englishclub.com/reading/environment/honeybee-reading.htm
to run away;uciekać
to buzz;brzęczeć
to sting;żądlić
a sting;żądło/użądlenie
to hurt;boleć/ranić
a venom;jad
to pollinate;zapylać
pollinated;zapylony
a pollinator;zapylacz
to rely on;zależeć od
a mystery;tajemnica
mysterious;tajemniczy
mysteriously;tajemniczo
some;niektórzy
many;wielu
most;większość
a beekeeper;pszczelarz
a hive;ul
a blueberry;jagoda
an almond;migdał
a cherry; czereśnia
to skyrocket;wystrzelić w kosmos
poor;biedny
poorer;biedniejszy
the poorest;najbiedniejszy
to suffer;cierpieć
bad;zły
worse;gorszy
the worst;najgorszy
a lack of something;niedobór czegoś
