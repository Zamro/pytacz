﻿!lng;español/hiszpański;polaco/polski
!spe;¿¡áÁéÉíÍñÑóÓúÚüÜ;ąĄćĆęĘłŁóÓśŚżŻźŹ
la ración;porcja
las patatas bravas;odważne ziemniaki
las albóndigas;pulpety
las gambas a la plancha;krewetki z rusztu
las sardinas fritas;smażone sardynki
los calamares a la romana;kalmary w panierce
los pimientos fritos;smażone papryczki
las aceitunas;oliwki
el ajo;czosnek
el refresco;napój orzeźwiający
la caña;małe piwo
la jarra;kufel
el vino tinto;czerwone wino
muy rico;bardzo dobry
caliente;gorący
frío;zimny
poder;móc
probar;próbować
dormir;spać
volver;wracać