﻿!lng;español/hiszpański;polaco/polski
!spe;¿¡áÁéÉíÍñÑóÓúÚüÜ;ąĄćĆęĘłŁóÓśŚżŻźŹ
el pan;chleb
la mantequilla;masło
el queso;ser
la leche;mleko
el jamón;szynka
la lechuga;sałata
la patata;ziemniak
la pasta;makaron
la carne;mięso
el pescado;ryba
el pollo;kurczak
la manzana;jabłko
el plátano;banan
los huevos;jajka
dulce;słodki
salado;słony
sano;zdrowy
tener alergia;mieć alergię
ser vegetariano;być wegetarianinem
a menudo;często
raras veces;rzadko
nunca;nigdy