﻿!lng;español/hiszpański;polaco/polski
!spe;¿¡áÁéÉíÍñÑóÓúÚüÜ;ąĄćĆęĘłŁóÓśŚżŻźŹ

decidir;decydować
decido;decyduję
decides;decydujesz
decide;decyduje
decidimos;decydujemy
decidís;decydujecie
deciden;decydują

vender;sprzedawać
vendo;sprzedaję
vendes;sprzedajesz
vende;sprzedaje
vendemos;sprzedajemy
vendéis;sprzedajecie
venden;sprzedają

tener;mieć
tengo;mam
tienes;masz
tiene;ma
tenemos;mamy
tenéis;macie
tienen;mają

ser;być
soy;jestem
eres;jesteś
es;jest
somos;jesteśmy
sois;jesteście
son;są