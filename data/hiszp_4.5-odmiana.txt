﻿!lng;español/hiszpański;polaco/polski
!spe;¿¡áÁéÉíÍñÑóÓúÚüÜ;ąĄćĆęĘłŁóÓśŚżŻźŹ
yo amo;ja kocham
tú amas;ty kochasz
él ama;on kocha
ella ama;ona kocha
usted ama;pan/pani kocha
nosotros amamos;my kochamy
vosotros amáis;wy kochacie
ellos aman;oni kochają
ellas aman;one kochają
ustedes aman;państwo kochają

yo bailo;ja tańczę
tú bailas;ty tańczysz
él baila;on tańczy
ella baila;ona tańczy
usted baila;pan/pani tańczy
nosotras bailamos;my(panie) tańczymy
vosotras bailáis;wy(panie) tańczycie
ellos bailan;oni tańczą
ellas bailan;one tańczą
ustedes bailan;państwo tańczą

beso;całuję
besas;całujesz
besa;całuje
besamos;całujemy
besáis;całujecie
besan;całują

canto;śpiewam
cantas;śpiewasz
canta;śpiewa
cantamos;śpiewamy
cantáis;śpiewacie
cantan;śpiewają

estudio;studiuję
estudias;studiujesz
estudia;studiuje
estudiamos;studiujemy
estudiáis;studiujecie
estudian;studiują

trabajo;pracuję
trabajas;pracujesz
trabaja;pracuje
trabajamos;pracujemy
trabajáis;pracujecie
trabajan;pracują

hablo;mówię
hablas;mówisz
habla;mówi
hablamos;mówimy
habláis;mówicie
hablan;mówią

compro;kupuję
compras;kupujesz
compra;kupuje
compramos;kupujemy
compráis;kupujecie
compran;kupują

me llamo;nazywam się
te llamas;nazywasz się
se llama;nazywa się
nos llamamos;nazywamy się
os llamáis;nazywacie się
se llaman;nazywają się

me peino;czeszę się
te peinas;czeszesz się
se peina;czesze się
nos peinamos;czeszemy się
os peináis;czeszecie się
se peinan;czeszą się
